# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import os
import random


filename = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        "bouts.txt")


def pipoter():
    with open(filename, 'r') as f:
        repliques = [r.splitlines() for r in f.read().split("\n\n")]
    l = []
    for i, r in enumerate(repliques):
        l.append(random.choice(r))
        if i == 4 and l[4][0] in 'aeiouyhéè':
            l[3] = l[3][:-2] + "'"
    return ''.join(l)


if __name__ == "__main__":
    print(pipoter())
